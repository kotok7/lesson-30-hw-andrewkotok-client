package org.client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket socket;
    private BufferedReader bReader;
    private BufferedWriter buffWriter;
    private String nickName;

    public Client(Socket socket, String name) {
        try {
            this.socket = socket;
            this.buffWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.bReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.nickName = name;


        } catch (IOException e) {
            closeAll(socket, bReader, buffWriter);
        }
    }

    public void sendMessage() {
        try {
            buffWriter.write(nickName);
            buffWriter.newLine();
            buffWriter.flush();
            Scanner sc = new Scanner(System.in);
            while (socket.isConnected()) {
                String messageToSend = sc.nextLine();
                buffWriter.write(nickName + ": " + messageToSend);
                buffWriter.newLine();
                buffWriter.flush();

            }
        } catch (IOException e) {
            closeAll(socket, bReader, buffWriter);

        }
    }

    public void readMessage() {
        new Thread(() -> {
            String msgFromChat;
            while (socket.isConnected()) {
                try {
                    msgFromChat = bReader.readLine();
                    System.out.println(msgFromChat);
                } catch (IOException e) {
                    closeAll(socket, bReader, buffWriter);
                }
            }
        }).start();
    }

    public void closeAll(Socket socket, BufferedReader buffReader, BufferedWriter buffWriter) {
        try {
            if (buffReader != null) {
                buffReader.close();
            }
            if (buffWriter != null) {
                buffWriter.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

}
